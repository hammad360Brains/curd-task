import React from "react";
import {
    Switch,
    Route,
    BrowserRouter as Router,
} from "react-router-dom";
import EditForm from "./components/screens/EditForm";
import FormRecord from "./components/screens/FormRecord";
import Home from './components/screens/Home'



const Routing = () => {
    return (
        <Router>
            <Switch>
                <Route exact path="/">
                    <Home />
                </Route>
                <Route exact path="/allreacord">
                    <FormRecord />
                </Route>
                <Route exact path="/editForm/:id">
                    <EditForm />
                </Route>
            </Switch>
        </Router>
    );
};

export default Routing;
