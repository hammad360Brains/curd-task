import axios from 'axios';

//add Form data in database

export const addInformation = (savedata) => {
    const config = {
        headers: {
            "Content-Type": "application/json",
        },
    };

    return axios.post('/api/addform', savedata, config);
}

//update record 

export const updateRecord = (id, updateData) => {
    const config = {
        headers: {
            "Content-Type": "application/json",
        },
    };

    return axios.post(`/api/edit/${id}`, updateData, config);
}


//get all form data

export const getallFormData = async () => {

    const response = await axios.get('/api/getAll');

    return response;
};

//for delete record
export const deleteOneRecord = (id) => {
    const config = {
        headers: {
            "Content-Type": "application/json",
        },
    };

    return axios.post(`/api/delete/${id}`, null, config);
}


export const featchData = async (id) => {
    const response = await axios.get(`/api/read/${id}`);
    // console.log(response)
    return response;

}