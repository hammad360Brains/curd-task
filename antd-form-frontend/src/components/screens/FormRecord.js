import React, { useEffect, useState, useCallback } from 'react'
import { useHistory } from 'react-router-dom'
import { Button, Table } from 'antd';
import Navbar from '../Header/Navbar';
import { deleteOneRecord, getallFormData } from '../api/formapi';
const FormRecord = () => {

    const [featchALLFormInformation, setFeatchALLFormInformation] = useState()
    const [loadDataAfterDelete, setLoadDataAfterDelete] = useState(false)
    const history = useHistory()



    // convert promise to over resposne and featch all data

    const loadFormInformation = useCallback(async () => {
        try {
            const { data } = await getallFormData();
            setFeatchALLFormInformation(data)
        } catch (error) {
            console.log("error found when fetch order data", error);
        }
    }, []);



    //function for delete record
    const deleteRecord = (id) => {
        deleteOneRecord(id)
        setLoadDataAfterDelete(!loadDataAfterDelete)
    }

    //function for edit Record
    const editRecord = (id) => {
        history.push(`/editForm/${id}`)

    }


    useEffect(() => {
        loadFormInformation()
    }, [loadFormInformation, loadDataAfterDelete])


    const columns = [
        {
            title: 'FirstName',
            dataIndex: 'firstname',
            key: 'firstname'
        },
        {
            title: 'LastName',
            dataIndex: 'lastname',
            key: 'lastname'
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email'
        },
        {
            title: 'Birthday',
            dataIndex: 'birthdaydate',
            key: 'birthday'
        },
        {
            title: 'Start Date',
            dataIndex: 'startdate',
            key: 'start data'
        },
        {
            title: 'End Date',
            dataIndex: 'enddate',
            key: 'end date'
        },
        {
            title: 'Grade',
            dataIndex: 'grade',
            key: 'grade'
        },
        {
            title: 'Edit',
            dataIndex: 'edit',
            key: 'edit',
            render: (text, record, index) => <Button
                onClick={() => editRecord(record._id)}
            > Edit</Button >
        },
        {
            title: 'Delete',
            dataIndex: 'delete',
            key: 'delete',
            render: (text, record, index) => <Button
                onClick={() => deleteRecord(record._id)}
            > Delete</Button >
        }
    ];


    const fromData = featchALLFormInformation && featchALLFormInformation



    return (
        <>
            <Navbar />
            <h1 align='center'>See All Records</h1>
            <Table
                columns={columns}
                dataSource={fromData}
                rowKey={record => record._id}
            />
        </>
    )
}

export default FormRecord
