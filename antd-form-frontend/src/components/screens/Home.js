import React, { useState } from 'react'
import 'antd/dist/antd.css'
import { DatePicker } from 'antd';
import { Form, Input, Button, Select } from 'antd';
import Navbar from '../Header/Navbar';
import {useHistory} from 'react-router-dom'
import { addInformation } from '../api/formapi';

const Home = () => {
    const [showDynamicForm, setShowDynamicForm] = useState(false)
    const [firstName, setFirstName] = useState()
    const [lastName, setLastName] = useState()
    const [email, setEmail] = useState()
    const [birthDayDate, setBirthDayDate] = useState()
    const [startDate, setStartDate] = useState()
    const [endDate, setEndDate] = useState()
    const [grade, setGrade] = useState()
    //for push to another Page
    const history = useHistory()

    const SubmiteData = () => {

        const saveDataInDb = {
            firstName,
            lastName,
            email,
            birthDayDate,
            startDate,
            endDate,
            grade
        }

        addInformation(saveDataInDb)
        history.push('/allreacord')
    
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    //getBirthdaydate

    const datePickerBirthDayDate = (value, dateString) => {
        setBirthDayDate(dateString);
    }

    //getStartdate

    const datePickerStartdate = (value, dateString) => {
        setStartDate(dateString);
    }
    //getenddate

    const datePickerEnddate = (value, dateString) => {
        setEndDate(dateString);
    }
    //show and hide form 
    const showForm = () => {
        setShowDynamicForm(!showDynamicForm)
    }
    //get selection data
    const SelectionOptions = (value) => {
        setGrade(value)
    }
    return (
        <>
            <Navbar />
            <h1 align="center">Add Information</h1>
            <Form name="basic"
                labelCol={{
                    span: 6,
                }}
                wrapperCol={{
                    span: 12,
                }}
                initialValues={{
                    remember: true,
                }}
                onFinish={SubmiteData}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item
                    label="First Name"
                    name="firstname"
                    onChange={(e) => setFirstName(e.target.value)}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your First Name!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="LastName"
                    name="lastname"
                    onChange={(e) => setLastName(e.target.value)}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Last Name!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Email"
                    name="email"
                    onChange={(e) => setEmail(e.target.value)}
                    rules={[
                        {
                            required: true,
                            type: "email",
                            message: 'Please input valid your email!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Birthday"
                    name="birthday"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your birthday Date!',
                        },
                    ]}
                >
                    <DatePicker onChange={datePickerBirthDayDate} />

                </Form.Item>
                <Form.Item
                    wrapperCol={{
                        offset: 6,
                        span: 6,
                    }}
                >
                    <Button type="primary" onClick={showForm}>
                        Academic
                    </Button>
                </Form.Item>
                {showDynamicForm ?
                    <>
                        <Form.Item
                            label="Start Date"
                            name="startdate"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your Start Date!',
                                },
                            ]}
                        >
                            <DatePicker onChange={datePickerStartdate} />

                        </Form.Item>
                        <Form.Item
                            label="End Date"
                            name="enddate"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your End Date!',
                                },
                            ]}
                        >
                            <DatePicker onChange={datePickerEnddate} />

                        </Form.Item>
                        <Form.Item
                            label="Grade"
                            name="grade"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your Grade!',
                                },
                            ]}
                        >
                            <Select onChange={SelectionOptions} >
                                <Select.Option value="primary">Primary</Select.Option>
                                <Select.Option value="secondary">Secondary</Select.Option>
                                <Select.Option value="12th">12th</Select.Option>
                            </Select>
                        </Form.Item>
                        <Form.Item
                            wrapperCol={{
                                offset: 6,
                                span: 1,
                            }}
                        >
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Form.Item>
                    </> : ''}
            </Form>
        </>
    )
}

export default Home
