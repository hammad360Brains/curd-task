import React, { useState, useEffect, useCallback } from 'react'
import 'antd/dist/antd.css'
import { DatePicker } from 'antd';
import { Form, Input, Button, Select } from 'antd';
import moment from 'moment';
import Navbar from '../Header/Navbar';
import { useHistory, useParams } from 'react-router-dom'
import { updateRecord, featchData } from '../api/formapi';


const EditForm = () => {
    const [form] = Form.useForm();
    const [firstName, setFirstName] = useState()
    const [lastName, setLastName] = useState()
    const [email, setEmail] = useState()
    const [birthDayDate, setBirthDayDate] = useState()
    const [startDate, setStartDate] = useState()
    const [endDate, setEndDate] = useState()
    const [grade, setGrade] = useState()
    const history = useHistory()
    const { id } = useParams()


    const readData = useCallback(async () => {
        try {
            const { data } = await featchData(id);
            //default value set
            defaultValues(data)

        } catch (error) {
            console.log("error found when fetch order data", error);
        }
    }, []);


    const defaultValues = (data) => {

        form.setFieldsValue({
            firstname: data.firstname,
            lastname: data.lastname,
            email: data.email,
            birthday: moment(data.birthdaydate),
            startdate: moment(data.startdate),
            enddate: moment(data.enddate),
            grade: data.grade

        });

        //defult value save in state if user not change any data defult value save in db 

        setFirstName(data.firstname)
        setLastName(data.lastname)
        setEmail(data.email)
        setBirthDayDate(data.birthdaydate)
        setStartDate(data.startdate)
        setEndDate(data.enddate)
        setGrade(data.grade)

    }



    useEffect(() => {
        readData()
    }, [readData, form, id])


    //submite Your data

    const SubmiteData = () => {

        const updateDataInDb = {
            firstName,
            lastName,
            email,
            birthDayDate,
            startDate,
            endDate,
            grade
        }
        updateRecord(id, updateDataInDb)
        history.push('/allreacord')

    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    //getBirthdaydate

    const datePickerBirthDayDate = (value, dateString) => {
        setBirthDayDate(dateString);
    }

    //getStartdate

    const datePickerStartdate = (value, dateString) => {
        setStartDate(dateString);
    }
    //getenddate

    const datePickerEnddate = (value, dateString) => {
        setEndDate(dateString);
    }
    //get selection data
    const SelectionOptions = (value) => {
        setGrade(value)
    }
    return (
        <>
            <Navbar />
            <h1 align="center">Edit Your  Information</h1>
            <Form
                name="basic"
                labelCol={{
                    span: 6,
                }}
                wrapperCol={{
                    span: 12,
                }}
                initialValues={{
                    remember: true,
                }}
                onFinish={SubmiteData}
                onFinishFailed={onFinishFailed}
                form={form}
            >
                <Form.Item
                    label="First Name"
                    name="firstname"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your First Name!',
                        },
                    ]}
                    onChange={(e) => setFirstName(e.target.value)}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="LastName"
                    name="lastname"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Last Name!',
                        },
                    ]}
                    onChange={(e) => setLastName(e.target.value)}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                        {
                            required: true,
                            type: "email",
                            message: 'Please input your valid email!',
                        },
                    ]}
                    onChange={(e) => setEmail(e.target.value)}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Birthday"
                    name="birthday"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your birthday Date!',
                        },
                    ]}
                >
                    <DatePicker onChange={datePickerBirthDayDate} />

                </Form.Item>
                <Form.Item
                    label="Start Date"
                    name="startdate"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Start Date!',
                        },
                    ]}
                >
                    <DatePicker onChange={datePickerStartdate} />

                </Form.Item>
                <Form.Item
                    label="End Date"
                    name="enddate"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your End Date!',
                        },
                    ]}
                >
                    <DatePicker onChange={datePickerEnddate} />

                </Form.Item>
                <Form.Item
                    label="Grade"
                    name="grade"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Grade!',
                        },
                    ]}
                    initialValue={grade}
                >
                    <Select onChange={SelectionOptions} >
                        <Select.Option value="primary">Primary</Select.Option>
                        <Select.Option value="secondary">Secondary</Select.Option>
                        <Select.Option value="12th">12th</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    wrapperCol={{
                        offset: 6,
                        span: 1,
                    }}
                >
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </>
    )
}

export default EditForm

