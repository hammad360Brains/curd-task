import React from 'react'
import { Menu} from 'antd';
import {Link} from 'react-router-dom'
const Navbar = () => {
    return (
        <div>
            <Menu mode="horizontal">
                <Menu.Item key="mail">
                    <Link to="/">Home</Link>
                </Menu.Item>
                <Menu.Item key="alipay">
                    <Link to="/allreacord">Table Data</Link>
                </Menu.Item>
            </Menu>

        </div>
    )
}

export default Navbar
