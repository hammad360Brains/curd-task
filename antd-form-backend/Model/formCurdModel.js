const mongoose = require('mongoose')


const formCurdSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: true,
    },
    lastname: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    birthdaydate: {
        type: String,
        required: true,
    },
    startdate: {
        type: String,
        required: true,
    },
    enddate: {
        type: String,
        required: true,
    },
    grade: {
        type: String,
        required: true,
    }
})

const TaskCurd = mongoose.model("Form", formCurdSchema)

module.exports = TaskCurd