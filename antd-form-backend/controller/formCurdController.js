const TaskCurd = require('../Model/formCurdModel');

exports.saveFormInDb = (req, res) => {
    try {
        const { firstName, lastName, email, birthDayDate, startDate, endDate, grade } = req.body

        if (!firstName || !lastName || !email || !birthDayDate || !startDate || !endDate || !grade) {
            return res.status(500).json("required All feilds")
        }

        const createSchema = new TaskCurd()
        createSchema.firstname = firstName
        createSchema.lastname = lastName
        createSchema.email = email
        createSchema.birthdaydate = birthDayDate
        createSchema.startdate = startDate
        createSchema.enddate = endDate
        createSchema.grade = grade


        createSchema.save()
            .then((data) => {
                res.status(200).json(data)
            })
            .catch((err) => {
                res.status(500).json("err while saving data", err)
            })

    } catch (error) {
        res.status(500).json("error to save information", error)
    }
}

exports.featchAllFormData = (req, res) => {
    try {
        TaskCurd.find({})
            .then((response) => {
                res.json(response)
            })
            .catch((err) => {
                res.json("error for featching data", err)
            })

    } catch (error) {
        res.json("error to featch data ", error)
    }
}

exports.deleteRecord = (req, res) => {
    try {
        TaskCurd.findByIdAndDelete(req.params.id)
            .then((response) => {
                res.status(200).json(response)
            })
            .catch((err) => {
                res.status(500).json(err)
            })

    } catch (error) {
        res.status(500).json("error ")
    }
}

exports.readEditableRecord = (req, res) => {
    try {
        TaskCurd.findById(req.params.id)
            .then((response) => {
                res.status(200).json(response)
            })
            .catch((err) => {
                res.status(500).json(err.message)
            })
    } catch (error) {
        res.status(500).json("error while read order", error.message)

    }
}

exports.updateRecord = (req, res) => {
    try {
        const { firstName, lastName, email, birthDayDate, startDate, endDate, grade } = req.body
        TaskCurd.findByIdAndUpdate(req.params.id,
            {
                $set:
                {
                    firstname: firstName,
                    lastname: lastName,
                    email,
                    birthdaydate: birthDayDate,
                    startdate: startDate,
                    enddate: endDate,
                    grade
                }

            },
            {
                new: true
            }
        )
            .then((response) => {
                res.status(200).json(response)
            })
            .catch((err) => {
                res.status(500).json("error update data ", err)
            })
    } catch (error) {
        res.status(500).json("error ", error)
    }
}