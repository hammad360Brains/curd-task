const express = require('express')
const app = express()
const dotenv = require('dotenv')
const connectDB = require('./database/Config')
dotenv.config();


app.use(express.json())


//routes
const formRoutes = require('./routes/formCurdroute')

//Db Connections
connectDB().then(() => {
    app.use('/', formRoutes)
    const port = process.env.PORT || 4000
    app.listen(port, () => {
        console.log(`your app is running on ${port} port`)
    })
}).catch(err => {
    console.log(`Database connection err:  ${err}`)
})


