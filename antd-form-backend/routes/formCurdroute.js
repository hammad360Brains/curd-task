const express = require('express');
const { saveFormInDb, featchAllFormData, deleteRecord, readEditableRecord, updateRecord } = require('../controller/formCurdController');
const router = express.Router()


router.post('/api/addform', saveFormInDb)
router.get('/api/getAll', featchAllFormData)
router.post('/api/delete/:id', deleteRecord)
router.get('/api/read/:id', readEditableRecord)
router.post('/api/edit/:id', updateRecord)



module.exports = router